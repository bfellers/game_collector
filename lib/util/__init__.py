import os
import random
import string


def make_random_file_name(length=32):
    char_pool = string.ascii_uppercase + string.digits
    file_name = "".join(random.choice(char_pool) for x in range(length))
    return file_name


def load_image(path=None):
    if path:
        return path
    else:
        return None


def get_file_extension(file=None):
    if file:
        fileName, fileExtension = os.path.splitext(file)
        return fileExtension


def add_image(file=None):
    if file:
        file_name = make_random_file_name()
        given_file = open(file, 'rb')
        new_file = open("img/{0}".format(file_name), 'wb')

        given_file_contents = given_file.read()
        new_file.write(given_file_contents)

        given_file.close()
        new_file.close()

        return file_name


def remove_image(image_id=None):
    if image_id:
        os.remove("img/{0}".format(image_id))
