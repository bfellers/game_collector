import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey, Date, Binary, Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

engine = create_engine('sqlite:///test', echo=True)
Session = sessionmaker(bind=engine)
session = Session()
engine.execute("select 1").scalar()

class Game(Base):
    __tablename__ = 'games'

    id = Column(Integer, primary_key=True)
    image = Column(Binary)
    title = Column(String)
    isbn = Column(String)
    location = Column(Integer, ForeignKey('friends.id'))
    notes = Column(String)
    platform = Column(Integer, ForeignKey('platforms.id'))
    price = Column(Float)
    publisher = Column(Integer, ForeignKey('companies.id'))
    rating = Column(Integer, ForeignKey('ratings.id'))
    release_date = Column(Date)
    score = Column(Integer)
    theme = Column(Integer, ForeignKey('themes.id'))
    description = Column(String)
    developer = Column(Integer, ForeignKey('companies.id'))
    genre = Column(Integer, ForeignKey('genres.id'))
    added_date = Column(Date)

    def __init__(self, title, genre=None):
        self.title = title
        self.genre = genre

    def __repr__(self):
        return "<Game('{0}, {1}, {2}')>".format(self.id, self.title,
                                                self.developer)


class Company(Base):
    __tablename__ = 'companies'

    id = Column(Integer, primary_key=True)
    title = Column(String)

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return "<Company({0}, {1})>".format(self.id, self.title)


class Friend(Base):
    __tablename__ = 'friends'

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def __repr__(self):
        return "<Friend({0}, {1},{2})>".format(self.id, self.last_name,
                                                self.first_name)


class Genre(Base):
    __tablename__ = 'genres'

    id = Column(Integer, primary_key=True)
    title = Column(String)

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return "<Genre({0}, {1})>".format(self.id, self.title)


class Platform(Base):
    __tablename__ = 'platforms'

    id = Column(Integer, primary_key=True)
    title = Column(String)

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return "<Platform({0}, {1})>".format(self.id, self.title)


class Rating(Base):
    __tablename__ = 'ratings'

    id = Column(Integer, primary_key=True)
    title = Column(String)

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return "<Rating({0}, {1})>".format(self.id, self.title)


class Theme(Base):
    __tablename__ = 'themes'

    id = Column(Integer, primary_key=True)
    title = Column(String)

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return "<Theme({0}, {1})>".format(self.id, self.title)

Base.metadata.create_all(engine)

# create game and add to db
halo = Game(title='Halo')
session.add(halo)
session.commit()

# query for new game
new_game = session.query(Game).filter_by(title='Halo').first()

# insert new company
bungie = Company('Bungie')
session.add(bungie)
session.commit()

halo.developer = bungie.id

session.add(halo)
session.commit()

halo2 = session.query(Game).filter_by(title='Halo').first()

print halo2.developer
