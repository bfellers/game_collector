import sqlite3
import logging

from lib.db.game import Game
from lib.util import add_image, remove_image

# LOGGING INFORMATION
LOG_FILE = 'log.txt'

# SQL CONSTANTS
INTEGER = 'INTEGER'
PRIMARY_KEY = 'INTEGER PRIMARY KEY AUTOINCREMENT'
TEXT = 'TEXT'

# TABLE DEFINITION
#
# DB is defined based on multi-level dictionary.
#
# Example:
# {'table_name': {'column_name': 'data_type'}}
#
# Note:
# Data types must be sqlite3 compatible.
TABLES = {'companies': {'company_id': PRIMARY_KEY,
                         'company_name': TEXT},
          'friends': {'friend_id': PRIMARY_KEY,
                      'friend_name': TEXT},
          'games': {'game_added_date': TEXT,
                    'game_description': TEXT,
                    'game_developer': INTEGER,
                    'game_genre': INTEGER,
                    'game_id': PRIMARY_KEY,
                    'game_image': TEXT,
                    'game_isbn': TEXT,
                    'game_location': INTEGER,
                    'game_notes': TEXT,
                    'game_platform': INTEGER,
                    'game_price': TEXT,
                    'game_publisher': INTEGER,
                    'game_rating': INTEGER,
                    'game_release_date': TEXT,
                    'game_score': INTEGER,
                    'game_theme': INTEGER,
                    'game_title': TEXT,
                    },
          'genres': {'genre_id': PRIMARY_KEY,
                     'genre_name': TEXT},
          'locations': {'location_id': PRIMARY_KEY,
                        'location_name': TEXT},
          'platforms': {'platform_id': PRIMARY_KEY,
                        'platform_name': TEXT},
          'ratings': {'rating_id': PRIMARY_KEY,
                      'rating_name': TEXT},
          'themes': {'theme_id': PRIMARY_KEY,
                     'theme_name': TEXT},
          }


class Collection():
    '''
    Contains all backend functionality for the game database.
    '''

    def __init__(self):
        '''
        setup connection to db and create tables here
        '''
        logging.basicConfig(filename=LOG_FILE, level=logging.DEBUG)

        self._db = sqlite3.connect('games')
        self._create_tables()

        self.companies = self.select_companies()
        self.friends = self.select_friends()
        self.games = self.select_games()
        self.genres = self.select_genres()
        self.locations = self.select_locations()
        self.platforms = self.select_platforms()
        self.ratings = self.select_ratings()
        self.themes = self.select_themes()

    def _create_tables(self):
        '''
        create any tables if they are not already in db
        '''
        cursor = self._db.cursor()

        # Create any tables defined above
        query_string = ""
        for table in TABLES:
            columns = TABLES[table].keys()
            columns.sort()
            for column in columns:
                query_string = "{0} {1} {2},".format(query_string, column, \
                               TABLES[table][column])
            query_string = "CREATE TABLE  IF NOT EXISTS {0} ({1})".format(\
                           table, query_string.rstrip(','))
            cursor.execute(query_string)
            self._db.commit()
            query_string = ""

        # We can also close the cursor if we are done with it
        cursor.close()

    def insert_company(self, company_name=None):
        '''
        insert company into database
        '''
        if company_name:
            sql = "INSERT INTO companies (company_name) VALUES (?)"
            args = (company_name,)
            return self._execute_sql(sql, args, get_last_row_id=True)

    def insert_friend(self, friend_name=None):
        '''
        insert friend into database
        '''
        if friend_name:
            sql = "INSERT INTO friends (friend_name) VALUES (?)"
            args = (friend_name,)
            return self._execute_sql(sql, args, get_last_row_id=True)

    def insert_game(self, game=None):
        '''
        logic for inserting new game into database
        '''
        if game:
            if game.get_image():
                game.set_image(add_image(game.get_image()))

            # If we already know about developer, justadd the id to the
            # record. Otherwise, insert the new company and get the id
            # to be inserted.
            if game.developer and game.developer in self.companies.keys():
                developer_id = self.companies[game.developer]
            else:
                developer_id = self.insert_company(game.developer)

            # If we already know about genre, just add the id to the
            # record. Otherwise, insert the new genre and get the id
            # to be inserted.
            if game.genre and game.genre in self.genres.keys():
                genre_id = self.genres[game.genre]
            else:
                genre_id = self.insert_genre(game.genre)

            sql = "INSERT INTO games (game_title, game_rating, game_image, " \
                  "game_developer, game_genre) " \
                  "values (?, ?, ?, ?, ?)"
            args = (game.get_title(), game.get_rating(), game.get_image(),
                    developer_id, genre_id)

            new_row_id = self._execute_sql(sql, args,
                                           get_last_row_id=True)

            game.set_id(new_row_id)

            return game

    def insert_genre(self, genre_name=None):
        '''
        Insert genre into database
        '''
        if genre_name:
            sql = "INSERT INTO genres (genre_name) VALUES (?)"
            args = (genre_name,)
            return self._execute_sql(sql, args, get_last_row_id=True)

    def insert_location(self, location_name=None):
        '''
        Insert location into database
        '''
        if location_name:
            sql = "INSERT INTO locations (location_name) VALUES (?)"
            args = (location_name,)
            return self._execute_sql(sql, args, get_last_row_id=True)

    def insert_platform(self, platform_name=None):
        '''
        Insert platform into database
        '''
        if platform_name:
            sql = "INSERT INTO platforms (platform_name) VALUES (?)"
            args = (platform_name,)
            return self._execute_sql(sql, args, get_last_row_id=True)

    def insert_rating(self, rating_name=None):
        '''
        Insert rating into database
        '''
        if rating_name:
            sql = "INSERT INTO ratings (rating_name) VALUES (?)"
            args = (rating_name,)
            return self._execute_sql(sql, args, get_last_row_id=True)

    def insert_theme(self, theme_name=None):
        '''
        Insert theme into database
        '''
        if theme_name:
            sql = "INSERT INTO theme (theme_name) VALUES (?)"
            args = (theme_name,)
            return self._execute_sql(sql, args, get_last_row_id=True)

    def select_companies(self):
        '''
        load any repetitive data into memory to reduce amount of disk access.
        '''
        sql = 'SELECT company_name, company_id FROM companies ' \
              'ORDER BY company_name'
        return dict(self._execute_sql(sql))

    def select_friends(self):
        '''
        load any repetitive data into memory to reduce amount of disk access.
        '''
        sql = 'SELECT friend_id, friend_name FROM friends ' \
              'ORDER BY friend_name'
        return dict(self._execute_sql(sql))

    def select_game(self, game=None):
        '''
        return game object based on id
        '''
        if game:
            sql = 'SELECT game_id, game_title, game_image, game_rating, ' \
                  'developers.company_name, publishers.company_name ' \
                  'FROM games ' \
                  'LEFT JOIN companies AS developers ' \
                  'ON games.game_developer=developers.company_id ' \
                  'LEFT JOIN companies AS publishers ' \
                  'ON games.game_publisher=publishers.company_id ' \
                  'WHERE game_id=?'
            args = (game.get_id(),)
            result = self._execute_sql(sql, args)

            if result:
                return Game(id=result[0][0],
                            title=result[0][1],
                            image=result[0][2],
                            rating=result[0][3],
                            developer=result[0][4],
                            publisher=result[0][5]
                            )

    def select_games(self):
        '''
        return list of game objects based on passed criteria
        '''
        sql = 'SELECT game_id, game_title, game_image, game_rating, ' \
              'developers.company_name, publishers.company_name ' \
              'FROM games ' \
              'LEFT JOIN companies AS developers ' \
              'ON games.game_developer=developers.company_id ' \
              'LEFT JOIN companies AS publishers ' \
              'ON games.game_publisher=publishers.company_id '

        results = self._execute_sql(sql)

        games = []
        if results:
            for result in results:
                new_game = Game(id=result[0],
                                title=result[1],
                                image=result[2],
                                rating=result[3],
                                developer=result[4])
                games.append(new_game)
        return games

    def select_genres(self):
        '''
        load any repetitive data into memory to reduce amount of disk access.
        '''
        sql = 'SELECT genre_name, genre_id FROM genres ' \
              'ORDER BY genre_name'
        return dict(self._execute_sql(sql))

    def select_locations(self):
        '''
        load any repetitive data into memory to reduce amount of disk access.
        '''
        sql = 'SELECT location_name, location_id FROM locations ' \
              'ORDER BY location_id'
        return dict(self._execute_sql(sql))

    def select_platforms(self):
        '''
        load any repetitive data into memory to reduce amount of disk access.
        '''
        sql = 'SELECT platform_name, platform_id FROM platforms ' \
              'ORDER BY platform_id'
        return dict(self._execute_sql(sql))

    def select_ratings(self):
        '''
        load any repetitive data into memory to reduce amount of disk access.
        '''
        sql = 'SELECT rating_name, rating_id FROM ratings ' \
              'ORDER BY rating_id'
        return dict(self._execute_sql(sql))

    def select_themes(self):
        '''
        load any repetitive data into memory to reduce amount of disk access.
        '''
        sql = 'SELECT theme_name, theme_id FROM themes ' \
              'ORDER BY theme_id'
        return dict(self._execute_sql(sql))

    def update_company(self, company_id=None, company_name=None):
        if company_id and company_name:
            sql = "UPDATE companies SET company_name=? WHERE company_id=?"
            args = (company_name, company_id)
            self._execute_sql(sql, args)

    def update_game(self, game=None):
        '''
        update game based on known game_id
        '''
        if game:
            sql = 'UPDATE games SET '
            args = ()
            if game.get_title():
                sql = sql + 'game_title=? '
                args = args + (game.get_title(),)
            sql = sql + 'WHERE game_id=? '
            args = args + (game.get_id(),)
            self._execute_sql(sql, args)
        else:
            return "game not found"

    def delete_games(self):
        '''
        delete all games in database
        '''
        sql = 'DELETE FROM GAMES;'
        self._execute_sql(sql)

    def _execute_sql(self, sql=None, args=None, get_last_row_id=False):
        '''
        @sql  string of sql syntax
        @args tuple of args to be fused in sql
        '''
        if sql:
            try:
                cursor = self._db.cursor()
                if args:
                    cursor.execute(sql, args)
                else:
                    cursor.execute(sql)
                self._db.commit()
                last_row_id = cursor.lastrowid
                results = cursor.fetchall()
                cursor.close()
                if get_last_row_id:
                    return last_row_id
                else:
                    return results
            except sqlite3.Error, e:
                print "An error occurred {error}".format(error=e.args[0])
