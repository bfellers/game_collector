from lib.util import load_image


class Game():
    def __init__(self,
                 description=None,
                 developer=None,
                 genre=None,
                 id=None,
                 image=None,
                 isbn=None,
                 location=None,
                 notes=None,
                 platform=None,
                 price=None,
                 publisher=None,
                 rating=None,
                 release_date=None,
                 score=None,
                 theme=None,
                 title=None):

        self.description = description
        self.developer = developer
        self.genre = genre
        self.id = id
        self.image = load_image(image)
        self.isbn = isbn
        self.location = location
        self.notes = notes
        self.platform = platform
        self.price = price
        self.publisher = publisher
        self.rating = rating
        self.release_date = release_date
        self.score = score
        self.theme = theme
        self.title = title

    def __repr__(self):
        '''
        What printing the object displays.
        '''
        output = "ID:{id}, Title:{title}, Rating:{rating}, " \
                 "Developer:{developer}"
        output = output.format(id=self.id,
                               title=self.title,
                               rating=self.rating,
                               developer=self.developer)
        return output

    def get_id(self):
        '''
        Return game id
        '''
        return self.id

    def get_title(self):
        '''
        Return game title
        '''
        return self.title

    def get_rating(self):
        '''
        Return game rating
        '''
        return self.rating

    def get_image(self):
        '''
        Return game image
        '''
        return self.image

    def set_id(self, id):
        '''
        @param id is new id value

        Set new game id.
        '''
        self.id = id
        return self.id

    def set_title(self, title, is_key=False):
        '''
        @param title is new title value
        @param is_key denotes whether new value is key or value

        Set game title
        return updated title

        add logic to update db record (maybe make commit function)
        '''
        self.title = title
        return self.title

    def set_rating(self, rating, is_key=False):
        '''
        @param rating is new rating value
        @param is_key denotes whether new value is key or value

        set game rating
        return updated rating

        make so you can pass

        add logic to update db record (maybe make commit function)
        '''
        self.rating = rating
        return self.rating

    def set_image(self, image):
        '''
        @param image is an image file

        set new image file

        make sure we add logic here to update db immediately
        '''
        self.image = image
        return self.image
