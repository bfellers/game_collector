###################################################################
# About

Goal is to allow user to store/categorize game collection

###################################################################
# What is tracked

- Game
  : Description
  : Developer
  : Genre
  : ISBN
  : Platform
  : Publisher
  : Rating
  : Release date
  : Theme
  : Title

  # Custom data
  : Date added
  : Image
  : Location (home, friends house, etc)
  : Notes
  : Score (x/10)
  : Purchased date
  : Purchased price

###################################################################
# Database

Using sqlite3.  Eventually want to add functionality to export to
xls, csv, etc.

###################################################################
# Contributers

Brad Fellers
