from lib.db.collection import Collection
from lib.db.game import Game


class Main():
    def __init__(self):
        # initialize db connections here
        self.collection = Collection()

    def test(self):
        '''
        test any existing functionality
        '''
        # insert test game
        new_game = self.collection.insert_game(Game(title="Halo",
                                                    rating=1,
                                                    image="halo.png",
                                                    developer='Bungie'))
        # Select all games
        print self.collection.select_games()

        # modify title of newly created game
        new_game.set_title('Halo 2')

        # update database with modified game
        self.collection.update_game(new_game)

        # Select game based on passed criteria
        print self.collection.select_game(new_game)

        # delete all games in db
        self.collection.delete_games()

        company_id = self.collection.insert_company(company_name="bungie")
        self.collection.update_company(company_id, company_name="activision")

main = Main()
main.test()
